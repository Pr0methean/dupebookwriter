﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;


namespace MinecraftBookWriter
{
    class Program
    {


        [DllImport("user32.dll")]
        internal static extern uint SendInput(uint nInputs, [MarshalAs(UnmanagedType.LPArray), In] INPUT[] pInputs, int cbSize);

#pragma warning disable 649
        internal struct INPUT
        {
            public UInt32 Type;
            public MOUSEKEYBDHARDWAREINPUT Data;
        }

        [StructLayout(LayoutKind.Explicit)]
        internal struct MOUSEKEYBDHARDWAREINPUT
        {
            [FieldOffset(0)]
            public MOUSEINPUT Mouse;
        }

        internal struct MOUSEINPUT
        {
            public Int32 X;
            public Int32 Y;
            public UInt32 MouseData;
            public UInt32 Flags;
            public UInt32 Time;
            public IntPtr ExtraInfo;
        }

#pragma warning restore 649


        public static void Click()
        {
            var inputMouseDown = new INPUT();
            inputMouseDown.Type = 0; /// input type mouse
            inputMouseDown.Data.Mouse.Flags = 0x0002; /// left button down

            var inputMouseUp = new INPUT();
            inputMouseUp.Type = 0; /// input type mouse
            inputMouseUp.Data.Mouse.Flags = 0x0004; /// left button up

            var inputs = new INPUT[] { inputMouseDown, inputMouseUp };
            SendInput((uint)inputs.Length, inputs, Marshal.SizeOf(typeof(INPUT)));

        }


        [StructLayout(LayoutKind.Sequential)]
        public struct Rect
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }

        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);

        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowRect(IntPtr hWnd, ref Rect rect);

        public static List<String> vs;
        public static Random rng = new Random();

        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        public static Byte randomByte()
        {
            return (Byte)rng.Next(0x00, 0xFF);
            
        }

   

        static string GetChar(int i)
        {
            return Encoding.UTF8.GetString(new byte[]
            {
        (byte) (i >> 16 & 0xFF),
        (byte) (i >> 8 & 0xFF),
        (byte) (i >> 0 & 0xFF),
            });
        }

        public static List<String> GenValidUnicodeTable()
        {
            var valid = new List<int>();

            for (int i = 0; i < 0x01000000; i++)
            {
                var s = Encoding.UTF8.GetString(new byte[]
                {
            (byte) (i >> 16 & 0xFF),
            (byte) (i >> 8 & 0xFF),
            (byte) (i >> 0 & 0xFF),
                });
                var c = Encoding.UTF8.GetByteCount(s);
                if (s.Length == 1 && c == 3)
                {
                    valid.Add(i);
                }
            }

            var validStrings = valid.Select(GetChar).ToList();

            return validStrings;
        }

        private static IntPtr getMinecraftWindowHandle()
        {
            foreach (Process pro in Process.GetProcessesByName("javaw"))
            {
                if (pro.MainWindowTitle.Contains("Delta") || pro.MainWindowTitle.Contains("Minecraft"))
                {
                    return pro.MainWindowHandle;

                }
            }
            return (IntPtr)0;
        }



        private static void moveCursor(int x, int y)
        {
            Cursor.Position = new Point(x, y);
        }

        static string GetRandomUnicodeString(int Max)
        {
            String UnicodeString = "";

            for (int ii = 0; ii <= Max; ii++)
            {
                UnicodeString += GetRandomUnicodeCharacter();
            }
            return UnicodeString;
        }
        static string GetRandomUnicodeCharacter()
        {
            string[] Unicodes = vs.ToArray();
            int i = rng.Next(0, Unicodes.Length);
            return Unicodes[i];
        }


        static void FocusMinecraft()
        {
            SetForegroundWindow(getMinecraftWindowHandle());
        }

        [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine("Generating Unicode Table\r\nPlease Wait. . .\n\n\nUTF-8 3 Bytes Table Generator © MysteryDash 2019");
            vs = GenValidUnicodeTable();
            Console.Clear();
            var rect = new Rect();
            GetWindowRect(getMinecraftWindowHandle(), ref rect);
            int width = rect.right - rect.left;
            int height = rect.bottom - rect.top;

            int Max;
            int Pages;
            int Delay;

            while (true)
            {
                Console.Clear();
                Console.Write("How many characters per page: (Recommended: 210) ");
                try
                {
                    Max = int.Parse(Console.ReadLine());
                }
                catch(Exception)
                {
                    Max = 210;
                }
                Console.Clear();
                Console.Write("How many pages to write: (Recommended: 50) ");
                try
                {
                    Pages = int.Parse(Console.ReadLine());
                }
                catch(Exception)
                {
                    Pages = 50;
                }
                
                Console.Clear();
                Console.Write("(LAG-Adjustment Settings)\nIf you have a laggy computer\nYou can specify a number of miliseconds\nTo wait before turning the page\nTo ensure that all the data is written to the book\nThis is particularly needed on 1.12.2 and lower.\nHow many Milliseconds (0 to disable)");
                try
                {
                    Delay = int.Parse(Console.ReadLine());
                }
                catch(Exception)
                {
                    Delay = 0;
                }
                
                Console.Clear();
                Console.WriteLine("Please make sure a book is open for editing in minecraft!\r\nPress enter to continue\r\n\r\nDont move or click the mouse while writing pages!");
                Console.ReadKey();
                Console.Clear();
                FocusMinecraft();
                Thread.Sleep(Delay);
                MoveWindow(getMinecraftWindowHandle(), 0, 0, 120, 250, true);
                for (int i = 1; i <= Pages; i++)
                {
                    Console.WriteLine("Writing Page " + i.ToString() + "...");
                    SendKeys.SendWait(GetRandomUnicodeString(Max));
                    Thread.Sleep(Delay);
                    moveCursor(100, 190);
                    Click();
                }
                moveCursor(50, 240);
                Click();
                Thread.Sleep(100);
                MoveWindow(getMinecraftWindowHandle(), rect.left, rect.top, width, height, true);
                FocusMinecraft();
                Console.Write("Done!\nPress Enter To Continue");
                Console.ReadKey();
            }


        }
    }
}
